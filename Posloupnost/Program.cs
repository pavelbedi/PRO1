﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Posloupnost
{
    class Program
    {
        static Random random = new Random();
        static void Main(string[] args)
        {
            int[] a = new int[10];  //0-9

            ///načtení hodnot do pole od uživatele
            //for (int i = 0; i < a.Length; i++)
            //{
            //    a[i] = Convert.ToInt32(Console.ReadLine());
            //}

            //Načtení hodnot do pole od 1 do N
            //for (int i = 0; i < a.Length; i++)
            //{
            //    a[i] = i + 1;
            //}

            //Načtení náhodných hodnot do pole
            for (int i = 0; i < a.Length; i++)
            {
                a[i] = random.Next(0, 11);
            }

            //vypsání posloupnosti
            VypisPosloupnost(a);

            //Součet posloupnosti
            int soucet = 0;
            int pocetSud = 0;

            for (int i = 0; i < a.Length; i++)
            {
                soucet += a[i];
            }

            Console.WriteLine("soucet = {0}", soucet);
            Console.WriteLine("prumer = {0}", soucet / (double)a.Length);

            //Počet sudých členů

            for (int i = 0; i < a.Length; i++)
            {
                if (a[i] % 2 == 0 && a[i] != 0)
                    pocetSud++;
            }
            Console.WriteLine("Počet sudých čísel = {0}", pocetSud);

            //Nalezení minimální hodnoty
            int min = a[0];
            for (int i = 1; i < a.Length; i++)
            {
                if (a[i] < min)
                    min = a[i];
            }
            //  Console.WriteLine("Minimum je = {0}", min);

            //počet minim
            int pocetMin = 0;
            for (int i = 0; i < a.Length; i++)
            {
                if (a[i] == min)
                    pocetMin++;
            }
            Console.WriteLine("Počet minim je = {0}", pocetMin);

            //první minimum
            int iMin = 0;

            for (int i = 1; i < a.Length; i++)
            {
                if (a[i] < a[iMin])  //poslední <=
                    iMin = i;

            }
            Console.WriteLine("Minimum je {0} a poprvé je na indexu {1}", min, iMin);


            //Odebrání člena posloupnosti
            Console.Write("Zadej index člena pro odebrání: ");
            //int k = Convert.ToInt32(ConsoleReadLine());
            int k = random.Next(a.Length);
            VypisPosloupnost(a);
            for (int i = k; i < a.Length - 1; i++)
            {
                a[i] = a[i + 1];
            }
            Array.Resize(ref a, a.Length - 1);  //Změní velikost pole A z N na N-1
            VypisPosloupnost(a);

            //Vložení nového člena do posloupnosti
            Console.Write("Zadej índex člena pro volžení: ");
            k = Convert.ToInt32(Console.ReadLine());
            Console.Write("Zadej hondotu nového člena: ");
            int h = Convert.ToInt32(Console.ReadLine());
            VypisPosloupnost(a);

            Array.Resize(ref a, a.Length + 1);
            for (int i = a.Length - 1; i > k; i--)
            {
                a[i] = a[i - 1];
            }
            a[k] = h;
            VypisPosloupnost(a);

            //cyklický posun vpřed (doleva)
            Console.WriteLine("cyklický posun vpřed (doleva)");
            for (int i = 0; i < a.Length; i++)
            {
                a[i] = i + 1;
            }
            VypisPosloupnost(a);

            int pom = a[0];
            for (int i = 0; i < a.Length - 1; i++)
            {
                a[i] = a[i + 1];
            }
            a[a.Length - 1] = pom;
            VypisPosloupnost(a);

            //cyklický posun vzad (doprava)
            Console.WriteLine("cyklický posun vzad (doprava)");
            pom = a[a.Length - 1];
            for (int i = a.Length - 1; i > 0; i--)
            {
                a[i] = a[i - 1];
            }
            a[0] = pom;

            VypisPosloupnost(a);
            Console.WriteLine();
            //Zkopírovat sudé členy z pole A do B
            int[] b = new int[0];
            for (int i = 0; i < a.Length; i++)
            {
                if (a[i] % 2 == 0)
                {
                    Array.Resize(ref b, b.Length + 1);
                    b[b.Length - 1] = a[i];
                }
            }
            Console.WriteLine("Pole A: ");
            VypisPosloupnost(a);
            Console.WriteLine("Pole B: ");
            VypisPosloupnost(b);

            //Spojení posloupnosti A a B do C

            int[] c = new int[a.Length + b.Length];
            for (int i = 0; i < a.Length; i++)
            {
                c[i] = a[i];
            }
            for (int i = 0; i < b.Length; i++)
            {
                c[a.Length+i] = b[i];
            }
            Console.WriteLine("Pole C: ");
            VypisPosloupnost(c);

            Console.ReadLine();
        }

        static void VypisPosloupnost(int[] a)
        {
            //vypsání posloupnosti
            for (int i = 0; i < a.Length; i++)

            {
                Console.Write("{0}, ", a[i]);
                //  Console.WriteLine("{0}. {1}", i, a[i]);
            }
            Console.WriteLine();


        }
    }
}
